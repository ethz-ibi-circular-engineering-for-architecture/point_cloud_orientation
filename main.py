import math
import numpy as np
import open3d as o3d
import os
import progressbar
import scipy.spatial as spatial

from tkinter import filedialog


def get_principal_components(cloud):
    mean, covariance = cloud.compute_mean_and_covariance()
    pcs = np.linalg.eig(covariance)
    return pcs


def normals_to_cloud(cloud):
    normals = np.asarray(cloud.normals)
    cloud_new = o3d.geometry.PointCloud()
    cloud_new.points = o3d.utility.Vector3dVector(normals)
    return cloud_new


def get_local_density(cloud):
    """Runs the input points in batches against the same KDTree as an attempt to avoid using too much memory,
    but that seemed to happen anyway """
    points = np.asarray(cloud.points)
    tree = spatial.KDTree(points)
    print("KDTree built")
    radius = 0.01
    radius = 0.005
    neighbor_counts = []

    batch_count = 100
    widgets = ["Processing Batches: ", progressbar.Percentage(), " ", progressbar.Bar(), " ", progressbar.ETA()]
    pbar = progressbar.ProgressBar(maxval=batch_count, widgets=widgets).start()

    batches = np.array_split(points, batch_count)
    for batch_id, batch in enumerate(batches):
        counts = tree.query_ball_point(batch, radius)
        counts = [len(n) for n in counts]
        neighbor_counts.extend(counts)
        pbar.update(batch_id)
    pbar.finish()

    return neighbor_counts


def get_v2v_rotation_matrix(a, b):
    """Returns the rotation matrix to rotate unit vector a onto unit vector b"""
    # From https://math.stackexchange.com/a/476311
    v = np.cross(b, a)
    #s = np.linalg.norm(v)
    c = np.dot(a, b)

    vx = [[0, -v[2], v[1]],
          [v[2], 0, -v[0]],
          [-v[1], v[0], 0]]

    vx2 = np.linalg.matrix_power(vx, 2)

    R = np.identity(3) + vx + (vx2 * (1 / (1 + c)))

    return R


if __name__ == '__main__':
    filepath = filedialog.askopenfilename(initialdir=os.getcwd(), title="Choose Input Cloud")
    dirname = os.path.dirname(filepath) + "/"
    basename = os.path.basename(filepath)

    vis = o3d.visualization.Visualizer()
    vis.create_window()

    # After downsampling, 200k points roughly takes 1 min processing time
    cloud = o3d.io.read_point_cloud(filepath)
    tread = math.ceil(len(cloud.points) / 200000)
    print(tread)
    cloud = cloud.uniform_down_sample(tread)
    print(cloud)
    normal_cloud = normals_to_cloud(cloud)
    colors = np.copy(np.asarray(normal_cloud.points))
    colors /= 2
    normal_cloud.colors = o3d.utility.Vector3dVector(colors)
    points = np.asarray(normal_cloud.points)

    #distances = np.asarray(normal_cloud.compute_nearest_neighbor_distance())
    # Add epsilon to account for zeros when taking log
    #distances += np.finfo(float).eps
    # Adjust for skew towards zero

    density = get_local_density(normal_cloud)
    #hist = np.histogram(density, 20)
    #print(hist)

    distances = np.asarray(density).astype(float)

    distances = np.log(distances)
    scale = 10
    distmax = np.max(distances)
    distances /= distmax
    distances *= scale
    # distances = scale - distances # Invert scale if necessary




    # Find first axis, being the vector with the greatest number of neighbors
    first_axis_id = np.argmax(distances)
    first_axis = points[first_axis_id]

    # Rotate cloud so that the first axis points at Z
    rotation_matrix = get_v2v_rotation_matrix(first_axis, np.asarray((0, 0, 1)))
    #print(rotation_matrix)
    points = np.matmul(points, rotation_matrix)

    # Take subset of cloud near the new ground plane
    filter = np.abs(points[:, 2]) < 0.1
    points = points[filter, :]
    colors = np.asarray(normal_cloud.colors)
    colors = colors[filter, :]
    normal_cloud.colors = o3d.utility.Vector3dVector(colors)
    distances = distances[filter]

    # Move points out by scaled distancescolor
    distances += 1
    distances = distances.reshape((len(distances), 1))
    points = np.multiply(points, distances)

    normal_cloud.points = o3d.utility.Vector3dVector(np.copy(points))
    vis.add_geometry(normal_cloud)

    # Find second axis, being the remaining vector with the greatest number of neighbors
    second_axis_id = np.argmax(distances)
    second_axis = points[second_axis_id]
    second_axis[2] = 0
    raw_length = np.linalg.norm(second_axis)
    #print(f"Raw Length : {raw_length}")
    second_axis /= raw_length

    rotation_matrix_2 = np.asarray([[second_axis[0], -second_axis[1], 0],
                                    [second_axis[1], second_axis[0], 0],
                                    [0, 0, 1]])
    #print(rotation_matrix_2)
    points = np.matmul(points, rotation_matrix_2)

    # Find principal components. Notes this was found to not be an appropriate method
    #pc = get_principal_components(normal_cloud)
    #print(pc)

    o3d.io.write_point_cloud("normal_cloud.ply", normal_cloud)

    rotation_matrix_final = np.matmul(rotation_matrix,rotation_matrix_2)
    #print(rotation_matrix_final)
    mstring = str(np.linalg.inv(rotation_matrix_final))
    mstring = mstring.replace("[", " ").replace("]", " ")
    #print(len(mstring))
    #print(list(mstring))
    mstring = mstring.replace("\n", " 0\n")
    mstring += "0 \r\n 0 0 0 1"
    print(mstring)
    #print(np.linalg.inv(rotation_matrix_final))



    mesh_frame = o3d.geometry.TriangleMesh.create_coordinate_frame(size=4, origin=[0, 0, 0])
    vis.add_geometry(mesh_frame)

    vis.run()
    vis.destroy_window()
