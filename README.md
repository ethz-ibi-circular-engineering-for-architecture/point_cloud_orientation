Point Cloud Orientation
=======================

This script computes the transformation matrix to align a point cloud with normals to the world axes. 
This works best with largely rectilinear scenarios, as significant angled or curvilinear forms may prevent a correct alignment or only align the ground plane (but not walls). 

Requirements
------------
The following python packages are used:  
  
- numpy  
- open3d  
- progressbar  
- scipy  
- tk  


Usage
-----
- Export the raw point cloud from Cloud Compare in the .ply format (binary mode is fine)  
- Run main.py from an Anaconda Prompt or IDE and select the exported .ply file when the file dialog appears  
- The point cloud is internally downsampled to 200k points, at this point, it may take several minutes to run  
- The script will print out a 4x4 matrix of numbers in the form:  
```
  -0.30943517 -0.79350072  0.52402866  0
  -0.87395354  0.45449839  0.1721522   0
  -0.37477307 -0.40470675 -0.83412055  0
 0 0 0 1
```
- Copy this matrix in to the Cloud Compare [Edit -> Apply Transformation] dialog. 